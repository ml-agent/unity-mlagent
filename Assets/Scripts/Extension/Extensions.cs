
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace SocialAnvil.Tools
{
    public static class Extensions
    {


        public static Transform[] GetAllChilds(this Transform relatedTransform)
        {
            List<Transform> trans = new List<Transform>();

            foreach(Transform t in relatedTransform)
            {
                if (t != relatedTransform)
                    trans.Add(t);
            }
            return trans.ToArray();
        }
        public static void ClearInstance(this GameObject gameobject)
        {
            SceneManager.MoveGameObjectToScene(gameobject, SceneManager.GetActiveScene());
        }

        public static Vector3 WithoutY(this Vector3 toMap)
        {
            return new Vector3(toMap.x, 0, toMap.z);
        }

        public static T GetRandomElement<T>(this IEnumerable<T> list)
        {
            // If there are no elements in the collection, return the default value of T
            if (list.Count() == 0)
                return default(T);

            return list.ElementAt(UnityEngine.Random.Range(0, list.Count()));
        }


        public static IEnumerable<T> GetValues<T>()
        {
            return Enum.GetValues(typeof(T)).Cast<T>();
        }

        public static void ClearChilds(this Transform _relatedTransform)
        {
            for (int i = _relatedTransform.childCount - 1; i >= 0; i--)
            {
                if (_relatedTransform != _relatedTransform.GetChild(i))
                {
                    MonoBehaviour.DestroyImmediate(_relatedTransform.GetChild(i).gameObject);
                }
            }
        }


        public static void Randomize<T>(T[] items)
        {
            System.Random rand = new System.Random();

            // For each spot in the array, pick
            // a random item to swap into that spot.
            for (int i = 0; i < items.Length - 1; i++)
            {
                int j = rand.Next(i, items.Length);
                T temp = items[i];
                items[i] = items[j];
                items[j] = temp;
            }
        }
    }
}
