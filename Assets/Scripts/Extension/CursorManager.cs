using SocialAnvil.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CursorManager : Singleton<CursorManager>
{
   

    public void SetCursor(bool useable)
    {
        Cursor.visible = useable;
        Cursor.lockState = useable ? CursorLockMode.None : CursorLockMode.Locked;
    }
}
