﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace SocialAnvil.Tools.Timer
{

    public class Timer
    {

        private bool init = false;
        private float TimeFinished;
        private float lastSetupedTime;
        public float LastSetupedTime => lastSetupedTime;
        private float lastTiming;
        public Timer()
        {
            //Debug.Log("New Timer");
        }
        public void SetTimer(float seconds)
        {
            lastSetupedTime = seconds;
            TimeFinished = Time.time + seconds;
            init = true;
            //   Debug.Log("Timer got Setuped: " +init);
            // Debug.Log("[Timer Debug] Timer set to: " + TimeFinished + " Current Time: " + Time.time);
        }
        public void DebugTime()
        {
            //   Debug.Log(TimeFinished - Time.time);
        }
        public bool isInited()
        {
            return init;
        }

        public bool isFinished()
        {
            if (TimeFinished <= Time.time)
            {
                //   Debug.Log("[Timer Debug] " + TimeFinished + "  >? " + Time.time);
                // Debug.Log("Timer Finished");
                return true;
            }
            //  Debug.Log("[Timer Debug] " + TimeFinished + "  > " + Time.time);
            return false;
        }
        public float GetOvertime()
        {
            return Time.time - TimeFinished;
        }

        public float GetProgress()
        {
            float per = GetReaminingTime() / lastSetupedTime;
            float right = 1 - per;
            return right;

        }

        public float GetReaminingTime()
        {
            return TimeFinished - Time.time;
        }
    }
}
