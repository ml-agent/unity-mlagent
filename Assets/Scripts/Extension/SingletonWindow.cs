﻿
using UnityEngine;
using UnityEngine.UI;

namespace SocialAnvil.Tools.UI
{
    ///-----------------------------------------------------------------
    ///   Namespace:      SocialAnvil.UI
    ///   Class:           WindowUI
    ///   Description:    Base functionallity for UI 
    ///   Author:         Konstantin Kühn                   Date: 08.11.2018
    ///   Notes:          Usefull to create UI 
    ///   Copyright Sovial Anvil UG(haftungebschränkt)
    ///-----------------------------------------------------------------
    public class SingletonWindowUI<T> : Singleton<T> where T : MonoBehaviour
    {
        protected Transform panel;

        public Button _closeButton;
        [SerializeField]
        protected bool _closeWithEscape;
        public override void Awake()
        {

            if (Instance != this)
            {
                DestroyImmediate(this.gameObject);
                return;
            }
            base.Awake();
            if (transform != null && transform.childCount > 0)
                panel = transform.GetChild(0);
            Hide();
            if (_closeButton != null)
            {
                _closeButton.onClick.AddListener(() =>
                {
                    Hide();
                });
            }
        }

        protected virtual void OnEnable()
        {
            if (transform != null && transform.childCount > 0)
                panel = transform.GetChild(0);
        }
        protected virtual void Update()
        {
            if (isOpen())
            {
                WhileIsOpen();
            }
        }

        public virtual void WhileIsOpen()
        {
            if (_closeWithEscape && Input.GetKeyDown(KeyCode.Escape))
            {
                _closeButton.onClick.Invoke();
            }
        }

        public virtual void Show()
        {
            if (isOpen())
                Hide();

            panel.gameObject.SetActive(true);


        }

        public virtual void Hide()
        {
            panel.gameObject.SetActive(false);
        }

        public virtual void Toggle()
        {
            if (isOpen())
                Hide();
            else
                Show();
        }


        public bool isOpen()
        {
            return panel.gameObject.activeInHierarchy;
        }

        public void DestroyChilds(Transform _transform)
        {
            foreach (Transform t in _transform)
            {
                if (t != _transform)
                {
                    Destroy(t.gameObject);
                }
            }
        }

        public virtual void RedrawUI()
        {

        }
    }
}
