using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using System.Linq;
using System.Collections.Generic;

public class Animal : Agent
{
    private SessionHandler _sessionHandler;
    private Rigidbody _AgentRb;

    [SerializeField]
    private AnimalSetting _settings;

    public float IncreaseLearnSpeed = 1;

    private float _feed;

    private int _age => StepCount;

    private float _health = 1;



    //First time the agent get called
    public override void Initialize()
    {
        base.Initialize();
        _AgentRb = GetComponent<Rigidbody>();
    }

    public void SetSession(SessionHandler sessionHandler)
    {
        _sessionHandler = sessionHandler;
    }
    public override void OnEpisodeBegin()
    {
        base.OnEpisodeBegin();
        _feed = 0;
        _health = _settings._maxHealth;

    }


    public override void CollectObservations(VectorSensor sensor)
    {
        sensor.AddObservation(_feed);
        sensor.AddObservation(_health);

    }


    public override void OnActionReceived(ActionBuffers actionBuffers)
    {

        if (_age >= _settings.MaxAge)
            _sessionHandler.AgentDied(this.gameObject);

        MoveAgent(actionBuffers.DiscreteActions);

        AddReward(-(IncreaseLearnSpeed / MaxStep));

        
        if (DoesLoseHP())
        {
            _health -= (_settings._maxHealth / (MaxStep / _settings.DeathRealativeToSteps));
            if (_health <= 0)
            {

                _sessionHandler.AgentDied(this.gameObject);
            }

        }
    }


    public void MoveAgent(ActionSegment<int> act)
    {
        var dirToGo = Vector3.zero;
        var rotateDir = Vector3.zero;

        var action = act[0];

        switch (action)
        {
            case 1:
                dirToGo = transform.forward * 1f;
                break;
            case 2:
                dirToGo = transform.forward * -1f;
                break;
            case 3:
                rotateDir = transform.up * 1f;
                break;
            case 4:
                rotateDir = transform.up * -1f;
                break;
            case 5:
                dirToGo = transform.right * -0.75f;
                break;
            case 6:
                dirToGo = transform.right * 0.75f;
                break;
        }
        transform.Rotate(rotateDir, Time.fixedDeltaTime * 200f);
        _AgentRb.AddForce(dirToGo * _settings.Speed,
            ForceMode.VelocityChange);
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("wall"))
        {
            AddReward(-_settings.Obstacle);
        }

        HandlePopulationCase(collision);

        HandleEatCase(collision);
    }

    private void HandleEatCase(Collision collision)
    {
        if (gameObject == null)
            return;
        if (_settings.Eatables.Any(x => collision.transform.CompareTag(x)))
        {
            _sessionHandler.AddGroupValue(gameObject.tag, 1f);
            _feed += _settings.FeedPerUse;
            _sessionHandler.Eat(collision.gameObject);
            _health += _settings._maxHealth / _settings.HealthOnEatRelative;
            if (_health > _settings._maxHealth)
                _health = _settings._maxHealth;
            AddReward(_settings.FeedReward);
        }
    }


    private bool CanIncreasePopulation()
    {
        return _feed >= _settings.MinFeedToPopulate;
    }

    private void HandlePopulationCase(Collision collision)
    {
        if (_settings.FriendlyUnits.Any(x => collision.transform.CompareTag(x)) && CanIncreasePopulation())
        {
            _feed -= _settings.PopulationCost;
            _sessionHandler.AddGroupValue(gameObject.tag, _settings.PopulateReward);

            AddReward(_settings.PopulateReward);
            _sessionHandler.IncreasePopulation(transform.position, gameObject.tag);
        }
    }


    private bool DoesLoseHP()
    {
        return _settings.DeathRealativeToSteps > 0;
    }
}
