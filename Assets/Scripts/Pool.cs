

using SocialAnvil.Tools;
using System.Collections.Generic;
using UnityEngine;

public class Pool : Singleton<Pool>
{

    private Dictionary<string, Queue<GameObject>> _pool = new Dictionary<string, Queue<GameObject>>();
    [SerializeField]
    private List<GameObject> _spawnable = new List<GameObject>();
    public override void Awake()
    {
        base.Awake();
    }

    public void Add(GameObject toPool, bool newScene = false)
    {
        var objectToSpawn = toPool.gameObject.tag;
        if (!_pool.ContainsKey(objectToSpawn))
            _pool.Add(objectToSpawn, new Queue<GameObject>());
        _pool[objectToSpawn].Enqueue(toPool);
        toPool.gameObject.SetActive(false);
        toPool.transform.SetParent(null);

    }

    public GameObject Spawn(string objectToSpawn, Transform related)
    {
        if (!_pool.ContainsKey(objectToSpawn))
            _pool.Add(objectToSpawn, new Queue<GameObject>());
     
        GameObject toReturn;
        if (_pool[objectToSpawn].Count == 0)
        {
            toReturn = Instantiate(_spawnable.Find(x => x.CompareTag(objectToSpawn)), related);
           
        }
        else
        {
            toReturn = _pool[objectToSpawn].Dequeue();

            toReturn.gameObject.SetActive(true);
            toReturn.transform.SetParent(related);
        }

        return toReturn;
    }
}
