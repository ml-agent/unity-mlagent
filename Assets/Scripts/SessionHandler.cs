
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Unity.MLAgents;

using UnityEngine;

public class SessionHandler : MonoBehaviour
{


    [SerializeField]
    private GameObject _ground;

    [HideInInspector]
    public Bounds areaBounds;



    [SerializeField]
    private AnimalSetting _animalSetting;

    [SerializeField]
    private int _startEntities, _startFood, _maxPopulation;
    [SerializeField]
    private List<GameObject> _SpawnedFeed = new List<GameObject>();
    private List<GameObject> _spawnedRabbits = new List<GameObject>();
    private List<GameObject> _spawnedWolfs = new List<GameObject>();
    private SimpleMultiAgentGroup _rabbits, _wolfs;

    [Header("Max Environment Steps")] public int MaxEnvironmentSteps = 25000;
    private bool _didWolfsExistAlready = false;
    private int _resetTimer;

    [SerializeField]
    private bool _useGroup;
    [SerializeField]
    private bool _usePredator;

    public int EndSessionReward;
    public int DeathPenatly;

    private void Start()
    {
        areaBounds = _ground.GetComponent<Collider>().bounds;
        if (_useGroup)
        {
            _rabbits = new SimpleMultiAgentGroup();

            _wolfs = new SimpleMultiAgentGroup();
        }

        StartSession();
    }

    private void FixedUpdate()
    {
        _resetTimer += 1;
        if (_resetTimer >= MaxEnvironmentSteps && MaxEnvironmentSteps > 0)
        {
            ResetScene(false);
            //Debug.Log("Iteration REached");
        }
        //Debug.Log($"Steps {_resetTimer}");

    }

    private void ResetScene(bool interrupt = false)
    {
        //Debug.Log("Reset Scene");
        StopAllCoroutines();
        _resetTimer = 0;
        StartSession();
        if (!interrupt)
        {
            if (_useGroup)
            {
                _rabbits.EndGroupEpisode();
                _wolfs.EndGroupEpisode();
            }
            else
            {
                _spawnedRabbits.ForEach(x => x.GetComponent<Agent>().EndEpisode());
                _spawnedWolfs.ForEach(x => x.GetComponent<Agent>().EndEpisode());
            }
        }
        else
        {
            if (_useGroup)
            {
                _rabbits.GroupEpisodeInterrupted();
                _wolfs.GroupEpisodeInterrupted();
            }
            else
            {
                _spawnedRabbits.ForEach(x => x.GetComponent<Agent>().EpisodeInterrupted());
                _spawnedWolfs.ForEach(x => x.GetComponent<Agent>().EpisodeInterrupted());
            }
        }
    }


    private IEnumerator SpawnPredators()
    {
        yield return new WaitForSeconds(UnityEngine.Random.Range(1, 2));
        if (_spawnedRabbits.Count > 2 && _spawnedWolfs.Count == 0)
        {
            for (int i = 0; i < 2; i++)
            {
                SetupAnimal(null, true);
                _didWolfsExistAlready = true;
            }
        }
    }


    private void StartSession()
    {
        //Debug.Log("Start new Session");
        ClearList(_SpawnedFeed);
        ClearList(_spawnedRabbits, true);
        ClearList(_spawnedWolfs, true);
        _didWolfsExistAlready = false;

        for (int i = 0; i < _startFood; i++)
        {
            SpawnGrass();
        }

        for (int i = 0; i < _startEntities; i++)
        {
            SetupAnimal();
        }
        if (_usePredator)
            StartCoroutine(SpawnPredators());
    }

    private void ClearList(List<GameObject> _entities, bool isAgent = false)
    {
        for (int i = 0; i < _entities.Count; i++)
        {
            GameObject entry = _entities[i];
            if (entry == null)
                continue;
            if (!_useGroup && isAgent)
                entry?.GetComponent<Agent>()?.EndEpisode();
            if (_useGroup)
            {
                if (entry.gameObject.CompareTag("rabbit"))
                    _rabbits.UnregisterAgent(entry.GetComponent<Agent>());
                if (entry.gameObject.CompareTag("wolf"))
                {
                    _wolfs.UnregisterAgent(entry.GetComponent<Agent>());
                }
            }
            Pool.Instance.Add(entry, true);
            //Destroy(entry);
        }
        _entities.Clear();

    }

    internal void AddGroupValue(string addTo, float value)
    {
        if (_useGroup)
        {
            if (addTo == "rabits")
                _rabbits.AddGroupReward(value);
            else if (addTo == "wolf")
                _wolfs.AddGroupReward(value);
        }
    }

    internal void IncreasePopulation(Vector3 position, string tag)
    {
        SetupAnimal(position, tag == "wolf");

        if (_maxPopulation <= _spawnedRabbits.Count + _spawnedWolfs.Count)
        {
            ResetScene();
        }
        if (_spawnedRabbits.Count > 2 && _spawnedWolfs.Count == 0 && _didWolfsExistAlready)
        {
            _spawnedRabbits.ForEach(x => x.GetComponent<Agent>().AddReward(EndSessionReward));
            ResetScene();
        }
        if (_spawnedRabbits.Count == 0)
        {
            ResetScene();
        }
    }

    public void Eat(GameObject gameObject)
    {
        Pool.Instance.Add(gameObject);
        if (gameObject.tag == "grass")
        {
            _SpawnedFeed.Remove(gameObject);
            Invoke(nameof(SpawnGrass), UnityEngine.Random.Range(2, 10));
        }
        else
        {
            AgentDied(gameObject);
        }

        if (_spawnedRabbits.Count == 0)
        {
            if (_useGroup)
            {
                _wolfs.AddGroupReward(EndSessionReward);
            }
            else
            {
                _spawnedWolfs.ForEach(x => x.GetComponent<Agent>().AddReward(EndSessionReward));
                ResetScene();
            }
        }

    }


    public void AgentDied(GameObject died, bool punishment = ture)
    {
        Pool.Instance.Add(died);
        if (died.tag == "wolf")
        {
            _spawnedWolfs.Remove(gameObject);

            if (_useGroup)
            {
                if (punishment)
                    _wolfs.AddGroupReward(-DeathPenatly);
                _wolfs.UnregisterAgent(died.GetComponent<Agent>());
            }
            else
            {
                if (punishment)
                    if (gameObject.TryGetComponent<Agent>(out Agent animal))
                    {
                        animal.AddReward(-DeathPenatly);
                    }
            }
        }
        else if (died.tag == "rabbit")
        {
            _spawnedRabbits.Remove(gameObject);
            if (_useGroup)
            {
                if (punishment)
                    _rabbits.AddGroupReward(-DeathPenatly);
                _rabbits.UnregisterAgent(died.GetComponent<Agent>());
            }
            else
            {
                if (gameObject.TryGetComponent<Animal>(out Animal animal))
                {
                    if (punishment)
                        animal.AddReward(-DeathPenatly);
                }
            }
        }

        if (_spawnedWolfs.Count == 0)
        {
            if (_useGroup)
            {

                _rabbits.AddGroupReward(EndSessionReward);
            }
            else
            {
                _spawnedRabbits.ForEach(x => x.GetComponent<Agent>().AddReward(EndSessionReward));
                ResetScene();
            }
        }
        else if (_spawnedRabbits.Count == 0)
        {
            if (_useGroup)
            {
                _wolfs.AddGroupReward(EndSessionReward);
            }
            else
            {
                _spawnedWolfs.ForEach(x => x.GetComponent<Agent>().AddReward(EndSessionReward));
                ResetScene();
            }
        }

    }
    private void SpawnGrass()
    {
        GameObject grass = Pool.Instance.Spawn("grass", transform);
        grass.transform.position = GetRandomSpawnPos();

        _SpawnedFeed.Add(grass);
    }


    private void SetupAnimal(Vector3? spawnLocation = null, bool isPredator = false)
    {
        Animal spawned = Pool.Instance.Spawn(isPredator ? "wolf" : "rabbit", transform).GetComponent<Animal>();
        spawned.transform.position = !spawnLocation.HasValue ? GetRandomSpawnPos() : spawnLocation.Value + new Vector3(0, 2, 0);
        spawned.transform.rotation = GetRandomRot();
        spawned.SetSession(this);
        if (isPredator)
        {
            _spawnedWolfs.Add(spawned.gameObject);
            if (_useGroup)
                _wolfs.RegisterAgent(spawned);
        }
        else
        {
            _spawnedRabbits.Add(spawned.gameObject);
            if (_useGroup)
                _rabbits.RegisterAgent(spawned);
        }
    }

    public Vector3 GetRandomSpawnPos()
    {
        var foundNewSpawnLocation = false;
        var randomSpawnPos = Vector3.zero;
        while (foundNewSpawnLocation == false)
        {
            var randomPosX = UnityEngine.Random.Range(-areaBounds.extents.x,
                areaBounds.extents.x);

            var randomPosZ = UnityEngine.Random.Range(-areaBounds.extents.z,
                areaBounds.extents.z);
            randomSpawnPos = _ground.transform.position + new Vector3(randomPosX, 1f, randomPosZ);
            if (Physics.CheckBox(randomSpawnPos, new Vector3(2.5f, 0.01f, 2.5f)) == false)
            {
                foundNewSpawnLocation = true;
            }
        }
        return randomSpawnPos;
    }

    Quaternion GetRandomRot()
    {
        return Quaternion.Euler(0, UnityEngine.Random.Range(0.0f, 360.0f), 0);
    }


}
