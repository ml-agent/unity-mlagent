using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="Settings/new Animal")]
public class AnimalSetting : ScriptableObject
{
    public float Speed;

    public int FeedReward;
    public int PopulateReward;
    public int EndState;
    public int Obstacle;

    public float _maxHealth = 1;

    public float DeathRealativeToSteps;

    public int MaxAge;

    public float HealthOnEatRelative;

    public float MinFeedToPopulate = 1;

    public float FeedPerUse = 1;

    public float PopulationCost = 1;


    //***** Tags *****

    public string[] FriendlyUnits, Eatables;
}
